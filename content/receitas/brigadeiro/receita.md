---
title: "(Sobremesa) Brigadeiro"
date: 2021-09-26T17:49:39-03:00
draft: false
---
![Brigadeiro](../brigadeiro.jpeg#center)

## Ingredientes

- 1 lata de **leite condensado**
- 3 colheres (sopa) de **chocolate em pó** ou de **cacau em pó**

&nbsp;

## Modo de preparo

Em uma panela, despeje o leite condensado e o chocolate em pó ou o cacau em pó.

Misture-os e aqueça a fogo baixo, mexendo sempre para não grudar no fundo e queimar.

Quando a mistura desgrudar do fundo da panela, ela está no ponto para um **brigadeiro de colher**. Note que a consistência ainda parece líquida, mas o brigadeiro irá endurecer ao resfriar. Basta armazená-lo em um recipiente e deixá-lo esfriar à temperatura ambiente. Você pode também decorar o seu brigadeiro com raspas de chocolate e chocolate em pó por cima!!

Para um **brigadeiro de enrolar**, espere a mistura engrossar um pouco. Depois, armazene-a em um recipiente e espere esfriar para poder começar a enrolar os brigadeiros. Passe granulado de chocolate sobre os brigadeiros e está pronto!