---
title: "(Vegetariano) Ratatouille"
date: 2021-09-26T17:49:39-03:00
draft: false
---

![Ratatouille](../ratatouille.jpeg#center)

## Ingredientes

- **Pimentão amarelo**
- **Tomate**
- **Cebola**
- **Berinjela**
- **Abobrinha italiana**
- 2 **alhos** picados
- 1/2 sachê de **molho de tomate**
- **sal**, **pimenta do reino**,  **orégano** e **azeite** (a gosto)

&nbsp;

## Modo de preparo

Corte os legumes em rodelas finas. Depois de cortada, deixe a berinjela descansar em um recipiente com água e sal por 10 minutos.

Cubra o fundo de uma forma com o molho de tomate. Depois, intercale todos os vegetais e, em seguida, espalhe o alho sobre os legumes. Acrescente o sal, a pimenta do reino e o azeite. 

Regue com o azeite e cubra com papel alumínio.

Por fim, leve ao forno (180 - 200 ºC) por 40 minutos.