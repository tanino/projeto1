---
title: "(Vegetariano) Strogonoff de Shimeji"
date: 2021-09-26T17:49:39-03:00
draft: false
---
![Strogonoff de Shimeji](../strogo.jpeg#center)

## Ingredientes

- 300 g de **shimeji**
- 1 colher (sopa) de **manteiga**
- 2 dentes de **alho** picados
- 1/2 **cebola** picada
- 3 colheres (sopa) de **molho inglês**
- 3 colheres (sopa) de **ketchup**
- 2 colheres (sopa) de **mostarda**
- 1 caixa de **creme de leite**
- **sal** a gosto

&nbsp;

## Modo de preparo

Numa frigideira, doure a cebola com a manteiga e depois acrescente o alho para dourar.

Adicione o shimeji e mexa até refogar.

Depois, acrescente o ketchup, a mostarda e o molho inglês e acerte o sal. Após incorporar os ingredientes, despeje o creme de leite.

Você pode adicionar algum acompanhamento, como arroz e batata palha!