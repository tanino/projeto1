---
title: "(Carnes) Sukiyaki"
date: 2021-09-26T17:49:39-03:00
draft: false
---
![Escondidinho](../sukiyaki.jpeg#center)

## Ingredientes

- **Shoyu**
- 1 xícara (chá) de **água**
- 1 envelope de **hondashi**
- 2 colheres (sopa) de **manteiga**
- 3 dentes de **alho** espremido
- 7 rodelas de **gengibre**
- 3 **ovos**
- 1 bandeja de **shimeji**
- 3 xícaras (chá) de **moyashi**
- 8 folhas de **acelga** picadas
- 300 g de **tofu** cortado em cubos
- 300 g de **carne** cortada em fatias finas

&nbsp;

## Modo de preparo

Em uma panela, derreta a manteiga e acrescente o alho. Refogado o alho, adicione o shoyu, a água,  o hondashi e o gengibre.

Acrescente os demais ingredientes, abafe com uma tampa e espere o cozimento.