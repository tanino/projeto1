---
title: "(Carnes) Torta de Frango"
date: 2021-09-26T17:49:39-03:00
draft: false
---
![Torta de Frango](../torta.jpeg#center)

## Ingredientes

### Massa
- 3 **batatas** cozidas
- 25 g de **parmesão ralado**
- 3 **ovos**
- 3/4 xícara (chá) de **óleo**
- 1 colher (chá) de **sal**
- 2 xícaras (chá) de **leite**
- 2 xícaras (chá) de **farinha de trigo**
- 1/2 xícara (chá) de **amido de milho**
- 1 colher (chá) de **fermento em pó**

### Recheio
- 1 **peito de frango** desfiado
- 2 **tomates**
- 1 **cebola**
- **Azeitonas**
- **Pimentão amarelo**
- **Cheiro verde**
- 1 pote de **requeijão**

&nbsp;

## Modo de preparo

Para a massa, esprema as batatas e acrescente os ovos, o leite e o óleo e misture. Depois, acrescente a farinha de trigo, o parmesão e o amido de milho e incorpore à massa. Por fim, adicione o fermento em pó.

Para o recheio, pique os tomates, a cebola, as azeitonas e o pimentão e acrescente-os ao frango desfiado, junto com o cheiro verde. Em seguida, adicione o requeijão e misture bem, sem levar ao fogo.

Unte uma assadeira e acrescente metade da massa. Depois, adicione o receio e o resto da massa.

Finalmente, leve ao forno (180 ºC) por 30 a 40 minutos.