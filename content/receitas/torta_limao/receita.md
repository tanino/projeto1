---
title: "(Sobremesa) Torta de Limão"
date: 2021-09-26T17:49:39-03:00
draft: false
---
![Torta de Limão](../torta.jpeg#center)

## Ingredientes

### Massa
- 1 pacote de **bolacha maizena** (200 g)
- 200 g de **manteiga sem sal**

### Recheio
- 1 lata de **leite condensado**
- 3 **limões**

&nbsp;

## Modo de preparo

Triture as bolachas no liquidificador até virar uma farofa. Depois, coloque em uma vasilha e acrescente a manteiga, e amasse com a mão até o ponto de poder moldar a massa.

Coloque a massa em uma forma com o fundo removível, cobrindo o fundo e as laterais. E leve ao forno (200 ºC) por 10 min para dar uma dourada na massa.

Para o recheio, coloque em uma vasilha o leite condensado e acrescente o suco dos três limões. Mexa bem até ficar com uma consistência homogênea.

Despeje o recheio sobre a massa. Você pode decorar a torta com raspas de limão.

Por fim, deixe 2 horas na geladeira e, depois, é só desenformar a torta!